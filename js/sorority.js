var sororityText = ["&Alpha;&Chi;&Omega;", "&Alpha;&Delta;&Pi;", "&Alpha;&Phi;", "&Chi;&Omega;",
					"&Delta;&Delta;&Delta;", "&Delta;&Gamma;", "&Delta;&Zeta;", "&Gamma;&Phi;&Beta;",
					"&Kappa;&Alpha;&Theta;", "&Kappa;&Delta;", "&Kappa;&Kappa;&Gamma;", "&Pi;&Beta;&Phi;",
					"&Sigma;&Delta;&Tau;", "&Sigma;&Kappa;", "&Sigma;&Sigma;&Sigma;", "&Zeta;&Tau;&Alpha;"];

var num_selected = 30;

/*
UI related funcitons
*/
window.onload = function() {
	var left_entries = document.getElementById("div_left_entries");
	var right_entries = document.getElementById("div_right_entries");
	var j;
	for(j = 0; j < num_selected; j++) {
		var textInput = document.createElement("div");
		textInput.addEventListener("drop", function(event) {
			drop(event);
		});
		textInput.addEventListener("dragover", function(event) {
			allowDrop(event);
		});

		textInput.id = "input_" + j;
		textInput.innerHTML = j + 1;
		textInput.classList.add("entry");
		if(j < num_selected / 2)
			left_entries.appendChild(textInput);
		else
			right_entries.appendChild(textInput);
	}
	setSelection(0);

}

/*
onkeypress event for the input region
*/
function enterInput(event) {
	$(".dropdown").slideToggle(300);
	var selected = null;


	var dropdown = document.getElementById("div_dropdown");
	while (dropdown.hasChildNodes()) {
    	dropdown.removeChild(dropdown.lastChild);
	}

	var names = getName(document.getElementById("input_entry").value);
	if(names == null)
		return;
	for(var i = 0; i < names.length; i++) {
		var nameDiv = document.createElement("div");
		nameDiv.id = names[i].replace(" ", "");
		nameDiv.innerHTML = names[i];
		nameDiv.draggable = "true";
		nameDiv.addEventListener("dragstart", function(event) {
			drag(event);
		});
		nameDiv.addEventListener("drop", function(event) {
			drop(event);
		});
		nameDiv.style.width = document.getElementById("input_0").style.width;
		nameDiv.style.height = document.getElementById("input_0").style.height;
		nameDiv.classList.add("draggable");
		dropdown.appendChild(nameDiv);
	}

}

function setSelection(slot) {
	document.getElementById("input_entry").focus();
	document.getElementById("input_entry").select();
}

/*
Shows the entire process of getting the preferences and random string and how they are used
to encrypt/decrypt the data
*/
function results() {
	var results = "";
	for(var i = 0; i < num_selected; i++) {
		if(document.getElementById("input_" + i).hasChildNodes) {
			results = results + document.getElementById("input_" + i).childNodes[0].id;
			if(i < num_selected - 1)
				results = results + ",";
		}
	}
	var random = getRandomString(results.length);
	var encoded = "";
    for(var i = 0; i < results.length; i++) {
      var a = results.charCodeAt(i);
      var b = a ^ random.charCodeAt(i);
      encoded = encoded + String.fromCharCode(b);
    }
    var original = "";
    for(var i = 0; i < encoded.length; i++) {
      var a = encoded.charCodeAt(i);
      var b = a ^ random.charCodeAt(i);
      original = original + String.fromCharCode(b);
    }
    console.log("Random: " + random);
	console.log("Results: " + results);
	console.log("Xor: " + encoded);
	console.log("original: " + original);
}

function getRandomString(length) {
  var array = new Uint8Array(length);
  var randomArray = window.crypto.getRandomValues(array);
  var string = "";
  for(var i = 0; i < randomArray.length; i++) {
    string += String.fromCharCode(randomArray[i]);
  }
  return string;
}

/*
Makes a call to the server where the names/computing ids are found
for both inputs of computingId and names, the server returns a json object
that we parse to create the tags with the id being the returned name with no spaces
*/
function getName(input) {
	if(input.length < 3)
		return;
	$.ajax({
		url: "https://localhost:8443/query",
		data: {
			para: input
		},
		type: "GET",
		dataType: "json",
	}).done(function(boon) {
		var dropdown = document.getElementById("div_dropdown");
		while (dropdown.hasChildNodes()) {
    		dropdown.removeChild(dropdown.lastChild);
		}
		var names = boon["names"].split(";");
		console.log("names length: " + names.length);
		for(var i = 0; i < names.length; i++) {
			var nameDiv = document.createElement("div");
			nameDiv.id = names[i].replace(" ", "");
			nameDiv.innerHTML = names[i];
			nameDiv.draggable = "true";
			nameDiv.addEventListener("dragstart", function(event) {
				drag(event);
			});
			nameDiv.addEventListener("drop", function(event) {
				drop(event);
			});
			nameDiv.style.width = document.getElementById("input_0").style.width;
			nameDiv.style.height = document.getElementById("input_0").style.height;
			nameDiv.classList.add("draggable");
			dropdown.appendChild(nameDiv);
		}
		return boon;
	})
	.fail(function( xhr, status, errorThrown ) {
    	console.log( "Error: " + errorThrown );
    	console.log( "Status: " + status );
    	console.dir( xhr );
  	});
}

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

/*
Change the element height to 100% so it fills the holder
*/
function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    if(ev.target.classList.contains("entry")) {
        ev.target.innerHTML = "";
        document.getElementById(data).style.height = "100%";
        ev.target.appendChild(document.getElementById(data));
    }

    for(var i = 0; i < num_selected; i++) {
    	var entry = document.getElementById("input_" + i);
    	if(entry.childElementCount == 0)
    		entry.innerHTML = i + 1;
    }
}

/*
  Sends the random string to the server
*/
function sendString(randomString) {
  $.ajax({
    type: "POST",
    url: "https://54.221.168.227:8443",
    data: {'crypt': randomString},
    success: function() {
      alert("succes");
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("failure");
    }
  });
}