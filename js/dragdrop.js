var sororityID = ["AlphaChiOmega", "AlphaDeltaPi", "AlphaPhi", "ChiOmega",
        "DeltaDeltaDelta", "DeltaGamma", "DeltaZeta", "GammaPhiBeta",
        "KappaAlphaTheta", "KappaDelta", "KappaKappaGamma", "PiBetaPhi",
        "SigmaDeltaTau", "SigmaKappa", "SigmaSigmaSigma", "ZetaTauAlpha"];

var randomString = "";
var encryptedString = "";

var dropdownText = "Please confirm that the selecions above are correct, then click submit.";

/*
  Sets up UI elements
*/
window.onload = function() {
  for(var i = 0; i < sororityID.length; i++) {
    var div = document.getElementById("div_" + sororityID[i]);
    div.onmouseenter = function() {
      this.style.opacity = 0.7;
    }
    div.onmouseleave = function() {
      this.style.opacity = 1;
    }
  }

  var holders = document.getElementsByClassName("holder");
  for(var i = 0; i < holders.length; i++) {
    holders[i].style.visibility = "visible";
  }

  var selection = document.getElementsByClassName("selection");
  for(var i = 0; i < selection.length; i++) {
    selection[i].style.visibility = "visible";
  }


  var div_submit = document.getElementById("div_submit");
  div_submit.style.opacity = "0.4";
  div_submit.onmouseenter = function() {
    if(this.style.opacity == 1)
      this.style.opacity = 0.7;
  }
  div_submit.onmouseleave = function() {
    if(this.style.opacity == 0.7)
      this.style.opacity = 1;
  }

}


function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

/*
Only the 3 selection divs and the divs originally holding the sorority elements
can respond to having elements dropped on them 
*/
function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    if(ev.target.classList.contains("selection") || ev.target.classList.contains("holder")) {
    	if(ev.target.classList.contains("selection"))
    		ev.target.innerHTML = "";
	    ev.target.appendChild(document.getElementById(data));
      fixSelection();
    }
}

/*
  Handles when a sorority tag is clicked (the element with the letters on it).
  The tag is moved to the first available preference slot.
*/
function sororityClicked(sorority) {
  for(var i = 0; i < 3; i++) {
    var selection = document.getElementById("div_selection_" + i);
    if(sorority.parentElement == selection) {
        var holders = document.getElementsByClassName("holder");
        for(var j = 0; j < holders.length; j++) {
            if(holders[j].childElementCount == 0) {
              holders[j].appendChild(sorority);
              fixSelection();
              return;
            }
        }
    }
  }

  //Set innerHTML to "" because otherwise the tag would be appended below the selection div
  for(var i = 0; i < 3; i++) {
    var selection = document.getElementById("div_selection_" + i);
    if(selection.childElementCount == 0) {
      selection.innerHTML = ""; //Change "" to a non-empty string if you do not understand the above comment
      selection.appendChild(sorority);
      fixSelection();
      return;
    }
  }
}

/*
  Returns an array of integers in order of preference with the values corresponding to the sorority name
  from sororityID. -1 is used if nothing is selected
*/
function getResults() {
  var results = [-1, -1, -1];
  var i;
  for(i = 0; i < sororityID.length; i++) {
    if(document.getElementById("div_selection_0").firstChild.id == "div_" + sororityID[i])
      results[0] = i;
    if(document.getElementById("div_selection_1").firstChild.id == "div_" + sororityID[i])
      results[1] = i;
    if(document.getElementById("div_selection_2").firstChild.id == "div_" + sororityID[i])
      results[2] = i;
  }
  return results[0] + "," + results[1] + "," + results[2];
}

function getRandomString(length) {
  var array = new Uint8Array(length);
  var randomArray = window.crypto.getRandomValues(array);
  var string = "";
  for(var i = 0; i < randomArray.length; i++) {
    string += String.fromCharCode(randomArray[i]);
  }
  return string;
}

/*
Shows the entire process of getting the preferences and random string and how they are used
to encrypt and decrypt the data
*/
function results() { 
    var results = getResults();
    if(randomString == "")
      randomString = getRandomString(results.length);
    sendString(randomString);
    var encoded = "";
    for(var i = 0; i < results.length; i++) {
      var a = results.charCodeAt(i);
      var b = a ^ randomString.charCodeAt(i);
      encoded = encoded + String.fromCharCode(b);
    }
    var original = "";
    for(var i = 0; i < encoded.length; i++) {
      var a = encoded.charCodeAt(i);
      var b = a ^ randomString.charCodeAt(i);
      original = original + String.fromCharCode(b);
    }

    console.log("random: " + randomString + "   length: " + randomString.length);
    console.log("results: " + results + "    length: " + results.length);
    console.log("xor: " + encoded + "    length: "  + encoded.length);
    console.log("original: " + original + "    length: " + original.length);

}


/*
A new window slides from the top of the screen when the submit button is clicked. The majority
of this function is for aesthetics.
*/
function slideDown() {
  var div_submit = document.getElementById("div_submit");
  if(div_submit.style.opacity == 0.4)
    return;
  
  var holders = document.getElementsByClassName("holder");
  for(var i = 0; i < holders.length; i++) {
    if(holders[i].style.visibility == "visible") {
      holders[i].style.visibility = "hidden";
      div_submit.innerHTML = "<span>Return</span>";
    } else {
      holders[i].style.visibility = "visible";
      div_submit.innerHTML = "<span>Submit</span>";
    }
  }

  var selection = document.getElementsByClassName("selection");
  for(var i = 0; i < selection.length; i++) {
    if(selection[i].style.visibility == "visible") {
      selection[i].style.visibility = "hidden";
    } else {
      selection[i].style.visibility = "visible";
    }
  }

  if(selection[0].style.visibility == "hidden") {
    document.getElementById("body").style.backgroundColor = "#555555";
  } else {
    document.getElementById("body").style.backgroundColor = "#EEEEEE";
  }

  var choices = [];
  for(var i = 0; i < 3; i++) {
    var div = document.getElementById("div_selection_" + i);
    if(div.childElementCount != 0)
      choices.push(div.firstChild.innerHTML);
  }
  var sororityChoices = "";
  for(var i = 0; i < choices.length; i++) {
    sororityChoices = sororityChoices + (i+1) + ". " + choices[i] + "<br/>";
  }

  document.getElementById("div_selection_confirm").style.fontFamily = "lead";
  document.getElementById("div_selection_confirm").innerHTML = sororityChoices + "<br>" + 
                                                      "<div id=\"div_confirm_button\" onclick=\"results()\">Submit</div>" + "<br><br>" + dropdownText;

  var btn_confirm = document.getElementById("div_confirm_button");
  btn_confirm.classList.add("btn-gradient");
  btn_confirm.style.width = "50%";
  btn_confirm.style.left = "25%";
  btn_confirm.style.position = "absolute";
  btn_confirm.style.opacity = 1;
  /*btn_confirm.onclick = function() {
    document.getElementById("div_submit_0").style.opacity = 1;
    this.style.opacity = 0.4;
  }*/

  btn_confirm.onmouseenter = function() {
    if(this.style.opacity == 1)
      this.style.opacity = 0.7;
  }
  btn_confirm.onmouseleave = function() {
    if(this.style.opacity == 0.7)
      this.style.opacity = 1;
  }

  //document.getElementById("div_submit_0").style.opacity = 0.4;
  //document.getElementById("div_submit_1").style.opacity = 0.4;
  document.getElementById("div_confirm_button").style.opacity = 1;

  $(".dropdown").slideToggle(300);
}

/*
  Sends the random string to the server
*/
function sendString(randomString) {
  $.ajax({
    type: "POST",
    url: "https://localhost:8443/post",
    data: {'crypt': randomString},
    success: function() {
      alert("succes");
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert("failure");
    }
  });
}

/*
If no element is in the selection, set the innerHTML back to "# choice"
*/
function fixSelection() {
    var divOne = document.getElementById("div_selection_0");
    if(divOne.childElementCount == 0)
      divOne.innerHTML = "1st choice";
    var divTwo = document.getElementById("div_selection_1");
    if(divTwo.childElementCount == 0)
        divTwo.innerHTML = "2nd choice";
    var divThree = document.getElementById("div_selection_2");
    if(divThree.childElementCount == 0)
        divThree.innerHTML = "3rd choice";
    if(divOne.childElementCount != 0 || divTwo.childElementCount != 0 || divThree.childElementCount != 0)
      document.getElementById("div_submit").style.opacity = "1";
    else
      document.getElementById("div_submit").style.opacity = "0.4";
}